<?php
    //Template Name: Noticias
        get_header()
    ?>
   <main id="noticias">
        <section id="sct-2">
            <div class="container">
                <h1>Últimas Notícias</h1>
                <div class="last-news">
                    <div class="card">
                        <h5>Título da Notícia</h5>
                        <p><?php the_field('manchete')?> </p>
                        <a href="#">Continuar Lendo</a> 
                    </div>
                    <div class="card">
                        <h5>Título da Notícia</h5>
                        <p><?php the_field('noticia1')?> </p>
                        <a href="#">Continuar Lendo</a> 
                    </div>
                    <div class="card">
                        <h5>Título da Notícia</h5>
                        <p><?php the_field('noticia2')?> </p>
                        <a href="#">Continuar Lendo</a> 
                    </div>
                    <div class="card">
                        <h5>Título da Notícia</h5>
                        <p><?php the_field('noticia3')?> </p>
                        <a href="#">Continuar Lendo</a> 
                    </div>
                    <div class="card">
                        <h5>Título da Notícia</h5>
                        <p><?php the_field('noticia4')?> </p>
                        <a href="#">Continuar Lendo</a> 
                    </div>
                    <div class="card">
                        <h5>Título da Notícia</h5>
                        <p><?php the_field('noticia5')?> </p>
                        <a href="#">Continuar Lendo</a> 
                    </div>
                </div>
                <a href="/todas-noticias.html">Ver Todas</a>
                <p style="display: none;">Não há notícias no momento!</p>
                <div class="card links-uteis">
                    <h4>Links Úteis</h4>
                    <ul>
                        <li><a href="https://oglobo.com">O Globo</a></li>
                        <li><a href="https://google.com">Google</a></li>
                        <li><a href="https://Twitter.com">Twitter</a></li>
                        <li><a href="https://facebook.com">Facebook</a></li>
                    </ul>
                </div>
            </div>
        </section>
    </main>
<?php
    get_footer()
?>