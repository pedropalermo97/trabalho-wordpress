<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l0YXrXsdQpWvkG4bsp2an9qin7m5d4ZlEPkAAtMsAayV7ZYaoQaXSZ9V5UAusi+iLF6SHBRrZ70Q2X0r7IvASA==');
define('SECURE_AUTH_KEY',  'eCH45E/mOVIO+B/7ExxktT5mpVKhTqtBd7S35q/ES1PV72dHv6Djzaur1ayJ/pfFknmt4LTNXLSW3LwUOuNjng==');
define('LOGGED_IN_KEY',    'YWmmOcF7/koVShdLhB/yoeFZVegz4qVNF5Bl44A6p6hlDJ2LFoke/X1vrw81/NiEfVG79AAL/65FJnbXMzDDxQ==');
define('NONCE_KEY',        'v2JnBPXvdN+wBH0ifTmp/c8f488bGvpHK6hA0IgOjbVpQHt8QEBCzu0e4h2pIoCVXSXO+yjXp2079kDYbt6v8w==');
define('AUTH_SALT',        'kQ8tQB8SxcaOTJ5VLRZeE6pEiJywndrRWJXXgeilHrhbMz7EwFM4AFTn3xjJ8lIkMZ+Zf1uFRcPQ3N7S484j6g==');
define('SECURE_AUTH_SALT', 'WEcQhCzcO+Sh9Y/z3a4UQyyNqRJ2BTd3zoNbX5J23zfg5fUNSk5W/rPzTOFmBnQBXakj20/ooAg7dmLxRUilzw==');
define('LOGGED_IN_SALT',   'pjDosgmPaXbsZOJoohhQFOOFYZcsP7VCPdP9VmRFRgrcl4UQW7UYOydmQKh/0/ZFIXAw+YT3z58X81G8+Ath+w==');
define('NONCE_SALT',       '+A5LtUSD4qNjjsbIt/lYAemHFsOqCUnrub1OaQXgB46US4JYfgavPIysXClKWCYpplvF+BJfi7AnK9btQKFcvQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
